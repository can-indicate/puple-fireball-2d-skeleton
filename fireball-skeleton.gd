extends Node2D


var time = 0.0


func _process(delta):
	var a = 5.0
	var b = 0.0
	time += delta*10
	var skeleton = get_node("/root/purple-fireball/Skeleton2D")
	var centre = skeleton.get_children()[0]
	var centre_transform = centre.get_transform()
	centre_transform.origin += Vector2(1.25*a*sin(time), -a*cos(time))
	var tail1 = centre.get_children()[3]
	var tail1_transform = tail1.get_transform()
	tail1_transform.origin += Vector2(0.0, a*cos(1.5*time))
	var tail2 = centre.get_children()[4]
	var tail2_transform = tail2.get_transform()
	tail2_transform.origin += Vector2(0.0, a*cos(1.5*time))
	var tailbone4 = tail2.get_children()[1]
	var tailbone4_transform = tailbone4.get_transform()
	tailbone4_transform.origin += Vector2(0.0, 1.5*a*cos(2.0*time))
	centre.set_transform(centre_transform)
	tail1.set_transform(tail1_transform)
	tail2.set_transform(tail2_transform)
	tailbone4.set_transform(tailbone4_transform)